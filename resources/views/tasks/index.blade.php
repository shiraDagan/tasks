
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<h1>This is a task list</h1>
@if (Request::is('tasks'))  

<h2><a href="{{action('TaskController@mytasks')}}">My Tasks</a></h2>
@else
<h2><a href="{{action('TaskController@index')}}">All Tasks</a></h2>

@endif

<ul>
  @foreach($tasks as $task)
  <li style="width:350px">
      <div style="display:flex;justify-content:space-between">
          <div>{{$task->title}}  </div>
          <div style="display:flex;justify-content:space-between; width:200px">
              <button class="fit"> <a class= "editTask removeLine" href = "{{route('tasks.edit',$task->id)}}"> edit</a> </button>
              @can('admin')  

              @if ($task->status)
                        <label id ="" value="1"> Done!</label>
                    @else
                        <button class="fit "  id ="{{$task->id}}" value="0"> Mark as done</button>
                    @endif
               <form method = 'POST' action ="{{action('TaskController@destroy', $task->id)}}">
               @csrf
                @method('DELETE')
                 <div class= "form-group" >
                 <input id="deleteb"  type = "submit" class= "form-control " name = "submit" value = "Delete">
                 </div>
                 </form>
                 @endcan
          </div>
    </div>
  </li> 
    @endforeach
    </ul>
    <button><a class= "create removeLine" href = "{{route('tasks.create')}}"> + new task</a></button>


    <script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
               location.reload();

           });

       });
   </script>

    <style>
    #editTask{
      background:blue;

    }
    
    #form-group{
    }
    .fit{
      height: fit-content;
    }
    #deleteb{
      background:red;
      color: white;
      border:none;
    }
    .create{
      margin-left:10px;
    }
    .removeLine{
      text-decoration: blink;
    }
    
    
    </style>